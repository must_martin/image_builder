Setup

```
$ pip install selenium
$ echo "deb http://dl.google.com/linux/chrome/deb/ stable main" | sudo tee -a /etc/apt/sources.list
$ wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
$ sudo apt-get update
$ sudo apt-get -y install libxpm4 libxrender1 libgtk2.0-0 libnss3 libgconf-2-4
$ sudo apt-get -y install google-chrome-stable
$ sudo apt-get install zip
$ wget https://chromedriver.storage.googleapis.com/2.45/chromedriver_linux64.zip
$ unzip chromedriver_linux64.zip
$ sudo apt-get -y install xvfb gtk2-engines-pixbuf
$ sudo apt-get -y install xfonts-cyrillic xfonts-100dpi xfonts-75dpi xfonts-base xfonts-scalable
```
