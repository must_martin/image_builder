from selenium import webdriver
from selenium.webdriver.chrome.options import Options


def run_chrome():
    options = Options()
    options.headless = True
    driver = webdriver.Chrome(options=options, executable_path=r'/home/vagrant/app/image_builder/chromedriver')
    driver.get("http://google.com/")
    driver.maximize_window()
    driver.save_screenshot('test.png')
    driver.quit()
